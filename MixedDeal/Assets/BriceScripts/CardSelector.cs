﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using System;


public class CardSelector : MonoBehaviour
{
    //The player hand size
    public static int handCount = 5;

    //Creating all the different decks of cards
    [SerializeField] private List<GameObject> deck;
    [SerializeField] private List<GameObject> playerHand;
    [SerializeField] private List<GameObject> graveyard;

    //Creating the deck of cards for the opponent
    [SerializeField] private List<GameObject> enemyDeck;
    [SerializeField] private List<GameObject> enemyHand;

    //General UI stuff
    [SerializeField] private TextMesh playerHp;
    [SerializeField] private TextMesh opponentHp;

    //Create the spawn points for the cards
    public GameObject _cardSpot1;
    public GameObject _cardSpot2;
    public GameObject _cardSpot3;
    public GameObject _cardSpot4;
    public GameObject _cardSpot5;
    public GameObject _playerCombatZone;
    public GameObject _enemyCombatZone;
    public GameObject _playerDeckArea;
    public GameObject _playerDeckBack;

    //Set the camera to the combat camera
    public Camera combatCamera;

    //Make a storage space for the cards
    public GameObject _Deck;

    //private int playerHealth = 20;

    public Image combatPanel;
    // Start is called before the first frame update
    void Start()
    {
        Instantiate(_playerDeckBack).transform.position = _playerDeckArea.transform.position;
        combatSetup();     
    }
    


    private List<GameObject> ShuffleList<GameObject>(List<GameObject> inputList)
    {
        List<GameObject> randomList = new List<GameObject>();

        System.Random r = new System.Random();
        int randomIndex = 0;
        while (inputList.Count > 0)
        {
            randomIndex = r.Next(0, inputList.Count); //Choose a random object in the list
            randomList.Add(inputList[randomIndex]); //add it to the new, random list
            inputList.RemoveAt(randomIndex); //remove to avoid duplicates
        }

        return randomList; //return the new random list
    }

    void Shuffle()
    {
        deck = ShuffleList(deck);
    }

    void ShuffleEnemy()
    {
        enemyDeck = ShuffleList(enemyDeck);
    }

    void drawCard()
    {
        GameObject card = (GameObject)Instantiate(deck[0]);
        playerHand.Add(card);
        deck.RemoveAt(0);
    }

    void playCard(GameObject card)
    {
        evaluateCombat(card);
        graveyard.Add(card);
        deck.Remove(card);
        drawCard(); 
    }
    
    void evaluateCombat(GameObject card)
    {
        enemyCardSelect();
        //int damageDealt = card.defense - enemyCard.damage;
        //if (damageDealt > 0)
        //{
        //    playerHealth = playerHealth - damageDealth;
        //}
    }

    void shuffleGraveyard()
    {
        while (graveyard.Count != 0) {
            deck.Add(graveyard[0]);
            graveyard.RemoveAt(0);
        }
        Shuffle();
    }

    void drawOpeningHand()
    {
        while (playerHand.Count < 5)
        {
            drawCard();
        }
        
        //Places all of the cards into the proper positions
        playerHand[0].transform.position = _cardSpot1.transform.position;
        playerHand[0].transform.SetParent(_Deck.transform);

        playerHand[1].transform.parent = _Deck.transform;
        playerHand[1].transform.position = _cardSpot2.transform.position;

        playerHand[2].transform.parent = _Deck.transform;
        playerHand[2].transform.position = _cardSpot3.transform.position;

        playerHand[3].transform.parent = _Deck.transform;
        playerHand[3].transform.position = _cardSpot4.transform.position;

        playerHand[4].transform.parent = _Deck.transform;
        playerHand[4].transform.position = _cardSpot5.transform.position;

    }

    void combatSetup()
    {
        Shuffle();
        ShuffleEnemy();
        drawOpeningHand();
        enemyCardSelect();
    }

    void enemyCardSelect()
    {
        GameObject enemyCard = (GameObject)Instantiate(enemyDeck[0]);
        enemyHand.Add(enemyCard);
        enemyHand[0].transform.position = _enemyCombatZone.transform.position;
        enemyHand[0].transform.SetParent(_Deck.transform);
        enemyDeck.RemoveAt(0);
    }

    //This whole section deals with the player selecting a card

    
    void playerCardSelect()
    {
    
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
